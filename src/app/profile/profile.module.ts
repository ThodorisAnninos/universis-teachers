import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { environment } from '../../environments/environment';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { SharedModule } from '@universis/common';
import { ProfileHomeComponent } from './components/profile-home/profile-home.component';
import { ProfileRoutingModule } from './profile-routing.module';
import { ProfilePreviewComponent } from './components/profile-preview/profile-preview.component';

import { NgPipesModule } from 'ngx-pipes';
import { NgArrayPipesModule } from 'angular-pipes';

// LOCALES: import extra locales here
import * as el from './i18n/profile.el.json';
import * as en from './i18n/profile.en.json';


@NgModule({
  imports: [
    CommonModule,
    ProfileRoutingModule,
    TranslateModule,
    NgPipesModule,
    NgArrayPipesModule,
    SharedModule
  ],
  declarations: [ProfileHomeComponent, ProfilePreviewComponent]
})
export class ProfileModule {
  constructor(private _translateService: TranslateService) {
    this._translateService.setTranslation("el", el, true);
    this._translateService.setTranslation("en", en, true);
  }
}
