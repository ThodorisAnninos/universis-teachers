import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ThesesAdvancedHomeComponent } from './theses-advanced-home.component';



describe('ThesesAdvancedHomeComponent', () => {
  let component: ThesesAdvancedHomeComponent;
  let fixture: ComponentFixture<ThesesAdvancedHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThesesAdvancedHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThesesAdvancedHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
