import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { RouterModalOkCancel } from '@universis/common/routing';
import { Subscription } from 'rxjs';
import { ThesesService } from '../../services/theses.service';
import { ErrorService, LoadingService } from '@universis/common';

@Component({
  selector: 'app-modal-details',
  templateUrl: './modal-details.component.html',
  styles: [`
  .modal-dialog {
    position: relative;
    max-width: 50%;
}

.inner__content {
    max-height: 70vh;
    overflow-y: auto;
}

.border-top {
    border-top: 2px solid #E4E7EA !important;
}

.border-right {
    border-right: 1px solid #E4E7EA !important;
}

.menu-left {
    list-style-type: none;
}

ul.menu-left li {
    cursor: pointer;
}

ul.menu-left li a {
    color: #333;
}

ul.menu-left li a.active {
    font-size: 18px;
}

.grade-title {
    font-family: inherit;
    font-weight: 500;
    line-height: 1.2;
    font-size: 1.5rem;
}

.passed-grade {
    color: #19D65B;
    font-size: 28px;
    font-weight: 600;
    letter-spacing: -0.76px;
    text-align: right;
    white-space: nowrap !important;
}

.failed-grade {
    color: #A4B7C1;
    font-size: 28px;
    font-weight: 600;
    letter-spacing: -0.76px;
    text-align: right;
    white-space: nowrap !important;
}

.grade-small {
    font-size: 24px !important;
}

.separator {
    box-sizing: border-box;
    height: 234px;
    width: 2px;
    border-left: 1px solid #C2CFD6;
    margin-left: 20px;
    margin-right: 20px;
}

.app-sidebar {
    width: 7%;
}

@media (max-width: 575.98px) {
    .inner__content {
        max-height: unset;
        overflow: hidden;
    }

    .modal-dialog {
        position: relative;
        max-width: 100%;
    }
}

.theses_search_filters {
    display: flex;
    flex-wrap: wrap;
    padding-top: auto;
    padding-bottom: auto;
    border: 1px solid #C2CFD6;
    border-radius: 6px;
    margin-top: 25px;
    background-color: #F0F3F5;
}

  `],

  encapsulation: ViewEncapsulation.None
})
export class ModalDetailsComponent extends RouterModalOkCancel implements OnInit, OnDestroy {
  private dataSubscription: Subscription;
  private routeSubscription: Subscription;

  public thesis: any = null;
  private student: number = null;
  public showActions: boolean = false;
  public isLoading: boolean = true;

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  constructor(private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _translateService: TranslateService,
    private _thesesService: ThesesService,
    private _loadingService: LoadingService,
    private _errorService: ErrorService) {
    super(_router, _activatedRoute);
    this.modalClass = 'modal-dialog';
  }

  ngOnInit() {
    this._loadingService.showLoading();
    this.isLoading = true;

    this.routeSubscription = this._activatedRoute.params.subscribe((params: any) => {
      if (params && params.id && params.studentId) {
        this.student = Number(params.studentId);
        // set a new property (studentResults) with the thesisResults for the selected student-thesis row
        this._thesesService.getStudentThesis(params.id, this.student).then(res => {
          res.studentResults = res.thesis.students.find(el => el.student.id === this.student);
          res.studentResults && res.studentResults.results && Array.isArray(res.studentResults.results) && res.studentResults.results.length > 0 
          ?  
          res.studentResults.results.forEach(el => {
            const instructor = res.thesis.members.find(item => item.member.id === el.instructor.id);
              el.instructor.roleName = instructor && instructor.roleName ? instructor.roleName : null;
          }) 
          : 
          res.studentResults;
          // sort the results so supervisor is first in the list
          res.studentResults.results.sort((a, b) => {
            return a.index < b.index ? -1 : 1;
          });

          this.thesis = res;
          this._loadingService.hideLoading();
          this.isLoading = false;
        }).catch(err => {
          return this._errorService.navigateToError(err);
        });
      }
    });
    this.cancelButtonClass = 'd-none';
  }

  cancel(): Promise<any> {
    return super.close();
  }

  ok(): Promise<any> {
    return super.close();
  }

}

