import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from '@universis/common';
import {ThesesCompletedComponent} from './components/theses-completed/theses-completed.component';
import {ThesesCurrentsComponent} from './components/theses-currents/theses-currents.component';
import {ThesesAdvancedHomeComponent} from './components/theses-advanced-home/theses-advanced-home.component';
import {ThesesAdvancedTableComponent} from './components/theses-advanced-table/theses-advanced-table.component';
import { ThesesAdvancedTableConfigurationResolver, ThesesAdvancedTableSearchResolver } from './components/theses-advanced-table/theses-advanced-table-config.resolver';
import { AdvancedFormModalData } from '@universis/forms';
import { ModalDetailsComponent } from './components/modal-details/modal-details.component';

const routes: Routes = [
  {
    path: '',
    component: ThesesCurrentsComponent,
    canActivate: [
      AuthGuard
    ],
    children: []
  },
  {
    path: 'current',
    component: ThesesCurrentsComponent,
    canActivate: [
      AuthGuard
    ],
    children: [ ]
  },
  {
    path: 'completed',
    component: ThesesCompletedComponent,
    canActivate: [
      AuthGuard
    ]
  },
  {
    path: 'advanced',
    canActivate: [
      AuthGuard
    ],
    component: ThesesAdvancedHomeComponent,
    children: [
        {
            path: '',
            pathMatch: 'full',
            redirectTo: 'list/current'
        },
        {
            path: 'list/:list',
            component: ThesesAdvancedTableComponent,
            resolve: {
                tableConfiguration: ThesesAdvancedTableConfigurationResolver,
                searchConfiguration: ThesesAdvancedTableSearchResolver
            },
            children: [
              {
                path: ':id/student/:studentId',
                component: ModalDetailsComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData>{
                  model: 'StudentThesis',
                  action: 'edit',
                  closeOnSubmit: true,
                  continueLink: '.',
                }
              }
            ]
        }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ThesesRoutingModule { }
