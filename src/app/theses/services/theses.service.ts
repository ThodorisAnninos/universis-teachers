import { Injectable } from '@angular/core';
import { AngularDataContext } from '@themost/angular';

@Injectable({
  providedIn: 'root'
})
export class ThesesService {

  constructor(private _context: AngularDataContext) { }

  getCurrentTheses(): any {
    return this._context.model('Instructors/Me/activetheses')
      .asQueryable()
      .expand('students,status($expand=locale),type($expand=locale),department($expand=locale),members($expand=member($select=InstructorSummary))')
      .orderByDescending('startYear')
      .thenByDescending('startPeriod')
      .take(-1)
      .getItems();
  }

  getCurrentThesesSelectValues(): any {
    return this._context.model('Instructors/Me/activetheses')
      .asQueryable()
      .expand('type($expand=locale)')
      .select('type')
      .take(-1)
      .getItems();
  }

  searchCurrentTheses(searchText: string = '') {
    return this._context.model('Instructors/Me/activetheses')
      .asQueryable()
      .expand('students,status($expand=locale),type($expand=locale),department($expand=locale),members($expand=member($select=InstructorSummary))')
      .where('name').contains(searchText)
      .orderByDescending('startYear')
      .thenByDescending('startPeriod')
      .take(-1)
      .prepare();
  }

  getCompletedTheses(): any {
    return this._context.model('Instructors/Me/theses')
      .asQueryable()
      .expand('students,status($expand=locale),type($expand=locale),department($expand=locale),members($expand=member($select=InstructorSummary))')
      .where('status/alternateName').equal('completed')
      .orderByDescending('startYear')
      .thenByDescending('startPeriod')
      .take(-1)
      .getItems();
  }

  getCompletedThesesSelectValues(): any {
    return this._context.model('Instructors/Me/theses')
      .asQueryable()
      .expand('type($expand=locale), status')
      .where('status/alternateName').equal('completed')
      .select('type')
      .take(-1)
      .getItems();
  }

  searchCompletedTheses(searchText: string = '') {
    return this._context.model('Instructors/Me/theses')
      .asQueryable()
      .expand('students,status($expand=locale),type($expand=locale),department($expand=locale),members($expand=member($select=InstructorSummary))')
      .where('status/alternateName').equal('completed')
      .and('name').contains(searchText)
      .orderByDescending('startYear')
      .thenByDescending('startPeriod')
      .take(-1)
      .prepare();
  }


  getStudentThesis(thesis: number, student: number) {
    return this._context.model('Instructors/Me/thesisStudents')
    .asQueryable()
    .expand('student($expand=studentStatus);$select=StudentSummary),thesis($expand=locale,instructor,startYear,startPeriod($expand=locale),status($expand=locale),type($expand=locale),department($expand=locale),students($expand=results),members($expand=member($select=InstructorSummary)))')
    .where('thesis').equal(thesis)
    .and('student').equal(student)
    .getItem();
  }

}
