import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConsultedStudentsTableComponent } from './components/consulted-students-table/consulted-students-table.component';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import { TablesModule } from '@universis/ngx-tables';
import { ConsultedStudentsRoutingModule } from './consulted-students-routing.module';
import { ConsultedStudentsHomeComponent } from './components/consulted-students-home/consulted-students-home.component';
// tslint:disable-next-line:max-line-length
import { ConsultedStudentsDefaultTableConfigurationResolver, ConsultedStudentsTableConfigurationResolver, ConsultedStudentsTableSearchResolver } from './components/consulted-students-table/consulted-students-table-config.resolver';
import { ConsultedStudentsSharedModule } from './consulted-students.shared';
import { MessagesSharedModule } from '../messages/messages.shared';
import { SharedModule } from '@universis/common';
import {RouterModalModule} from '@universis/common/routing';

// LOCALES: import extra locales here
import * as el from './i18n/consulted-students.el.json';
import * as en from './i18n/consulted-students.en.json';

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        ConsultedStudentsSharedModule,
        TablesModule,
        SharedModule,
        ConsultedStudentsRoutingModule,
        RouterModalModule
    ],
  declarations: [
    ConsultedStudentsHomeComponent,
    ConsultedStudentsTableComponent
  ],
  entryComponents: [
  ],
  providers: [
    ConsultedStudentsTableConfigurationResolver,
    ConsultedStudentsDefaultTableConfigurationResolver,
    ConsultedStudentsTableSearchResolver
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ConsultedStudentsModule {
  constructor(private _translateService: TranslateService) {
    this._translateService.setTranslation("el", el, true);
    this._translateService.setTranslation("en", en, true);
  }
}

